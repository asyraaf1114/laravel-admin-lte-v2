@extends('layouts.app')

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="/">       
            <b>ADMIN</b> LTE Version 2</a>      
        </a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">
            Register to start your session
        </p>

        @if(session('message'))
            <p class="alert alert-info">
                {{ session('message') }}
            </p>
        @endif

        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                <input id="name" type="text" name="name" class="form-control" required autocomplete="name" autofocus placeholder="Name" value="{{ old('name', null) }}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>

                @if($errors->has('name'))
                    <br>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('name') }}
                    </div>
                @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                <input id="email" type="email" name="email" class="form-control" required autocomplete="email" autofocus placeholder="Email" value="{{ old('email', null) }}">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                @if($errors->has('email'))
                    <br>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('email') }}
                    </div>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
                <input id="password" type="password" name="password" class="form-control" required placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if($errors->has('password'))
                    <br>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong><i class="icon fa fa-ban"></i> Alert!</strong> &nbsp; {{ $errors->first('password') }}
                    </div>
                @endif
            </div>
            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : '' }} has-feedback">
                <input id="password-confirm" type="password" name="password_confirmation" class="form-control" required placeholder="Confirm Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <a href="{{ route('login') }}">
                        I already have an account
                    </a><br>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                        Register
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection
