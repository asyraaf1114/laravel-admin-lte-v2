@extends('layouts.app')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="/">       
            <b>ADMIN</b> LTE Version 2</a>      
        </a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">
            Reset Password
        </p>

        @if(session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" required autocomplete="email" autofocus placeholder="Email" value="{{ old('email') }}">

                    @if($errors->has('email'))
                        <span class="help-block" role="alert">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-flat btn-block">
                            Send Password Reset Link
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection