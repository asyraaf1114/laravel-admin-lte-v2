@extends('layouts.app')
@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="/">       
            <b>ADMIN</b> LTE Version 2</a>      
        </a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">
            Reset Password
        </p>

        <form method="POST" action="{{ route('password.request') }}">
            @csrf

            <input type="hidden" name="token" value="{{ $token }}">

            <div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="Email">

                    @if($errors->has('email'))
                        <span class="help-block" role="alert">
                            {{ $errors->first('email') }}
                        </span>
                    @endif
                </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                    @if($errors->has('password'))
                        <span class="help-block" role="alert">
                            {{ $errors->first('password') }}
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-flat btn-block">
                            Reset Password
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection