
# Laravel Admin LTE V2 (Starter)

# clone
  - git clone https://gitlab.com/asyraaf1114/laravel-admin-lte-v2
  - cd laravel-admin-lte-v2

# copy .env.example to .env and setup your Database information.
  - cp .env.example .env

# composer install, key, migration and serve
  - composer install
  - php artisan key:generate
  - php artisan migrate
  - php artisan serve

# note
  please register first since there is no seeding
